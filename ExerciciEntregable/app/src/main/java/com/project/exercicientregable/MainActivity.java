package com.project.exercicientregable;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import static android.icu.text.Normalizer.YES;

public class MainActivity extends AppCompatActivity {

    EditText et1;
    EditText et2;
    EditText et3;
    EditText et4;

    boolean notEmpty = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et1 = findViewById(R.id.editText1);
        et2 = findViewById(R.id.editText2);
        et3 = findViewById(R.id.editText3);
        et4 = findViewById(R.id.editText4);

    }

    public void checkTextView(EditText variable1){
        if(variable1.equals("")){
            variable1.setError(getString(R.string.messageError));
            notEmpty = false;
        }
    }

    public void openDialog(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.titleAlert));
        alertDialogBuilder.setMessage(R.string.questionAlert)
                .setCancelable(false)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int id) {
                        deleteCamps();
                    }
                })
                .setNegativeButton(getString(R.string.btn_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create(); //crear el alert dialog
        alertDialog.show(); //mostrar per pantalla
    }

    private void deleteCamps() {
        et1.setText("");
        et2.setText("");
        et3.setText("");
        et4.setText("");
    }



    public void showTheCamps(View view) {
        if(notEmpty){

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("camp1", et1.getText().toString());
        intent.putExtra()

    }
}
